package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;
	PolylineOptions po;
	int CC;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		setContentView(R.layout.activity_main);
		
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		
		
		
		
		
	
		
		
		map.setMyLocationEnabled(true);
		
		
		
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				
				
				po = new PolylineOptions();
				
				po.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				po.add(new LatLng(l.getLatitude(),l.getLongitude()));
				po.color(CC);
				currentLocation = l;
				
				po.width(5);
				map.addPolyline(po);
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
			case R.id.mark:
				//
				MarkerOptions mo = new MarkerOptions();
				mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				mo.title("Hello");
				
				map.addMarker(mo);
				break;
				case R.id.ranc:
					//
					Random r = new Random();
					CC = Color.rgb(r.nextInt(255),r.nextInt(255),r.nextInt(255));
					
				
				
				
				break;
		
		}
			
			
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
	
	

}
